+++
title = "My first blog post"
author = ["CarlosPerez"]
date = 2018-12-02T20:53:00-05:00
draft = false
+++

This is the first blog post for my new Hugo Blog. I will use it to test
functionalities, therefore it will keep growing.

\\[ \frac{\hbar^2}{2m}\nabla^2\Psi + V(\mathbf{r})\Psi = -i\hbar
\frac{\partial\Psi}{\partial t} \\]